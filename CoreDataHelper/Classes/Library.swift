//
//  Library.swift
//  CoreDataHelper
//
//  Created by Артём Шляхтин on 20/01/2017.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import Foundation

public struct CoreDataHelperFramework {
    public static let bundleName = "ru.roseurobank.CoreDataHelper"
}
