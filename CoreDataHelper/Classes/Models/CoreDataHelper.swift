//
//  CoreDataHelper.swift
//  CoreDataHelper
//
//  Created by Артем Шляхтин on 05/02/16.
//  Copyright © 2016 Артем Шляхтин. All rights reserved.
//

import UIKit
import CoreData
import System

public enum CoreDataError: Error {
    case fetchFailed
    case saveFailed
}

open class CoreDataHelper: NSObject {
    
    
    // MARK: - Singletone
    
    public class var shared: CoreDataHelper {
        struct Singletone {
            static let instance = CoreDataHelper()
        }
        return Singletone.instance
    }
    
    
    // MARK: - Core Data Initialization
    
    fileprivate override init() {
        super.init()
        self.performInitialization()
    }
    
    
    // MARK: - Actions
    
    /**
     Возвращает упорядоченный массив объектов.
     
     - parameter entityName: Имя сущности.
     - parameter sort: Массив объектов описывающий сортировку.
     - returns: Массив с отсортированными объектами.
    */
    public func objects(entityName entity: String, sort: [NSSortDescriptor]?) -> [NSManagedObject] {
        return objects(entityName: entity, sort: sort, inContext: self.managedObjectContext)
    }
    
    /**
     Возвращает упорядоченный массив объектов.
     
     - parameter entityName: Имя сущности.
     - parameter sort: Массив объектов описывающий сортировку.
     - parameter inContext: Контекст в котором отрабатывает операция.
     - returns: Массив с отсортированными объектами.
    */
    public func objects(entityName entity: String, sort: [NSSortDescriptor]?, inContext context: NSManagedObjectContext) -> [NSManagedObject] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        request.fetchBatchSize = 100
        request.sortDescriptors = sort
        return fetchRequest(request, inContext: context)
    }
    
    /**
     Возвращает фильтрованный массив объектов.
     
     - parameter entityName: Имя сущности.
     - parameter field: Поле для фильтра.
     - parameter value: Значение для фильтра.
     - parameter sort: Массив объектов описывающий сортировку.
     - returns: Массив с отфильтрованными объектами.
    */
    public func objectsByFilter(entityName entity: String, field: String, value: AnyObject, sort: [NSSortDescriptor]?) -> [NSManagedObject] {
        return objectsByFilter(entityName: entity, field: field, value: value, sort: sort, inContext: self.managedObjectContext)
    }
    
    /**
     Возвращает фильтрованный массив объектов.
     
     - parameter entityName: Имя сущности.
     - parameter field: Поле для фильтра.
     - parameter value: Значение для фильтра.
     - parameter sort: Массив объектов описывающий сортировку.
     - parameter context: Контекст в котором отрабатывает операция.
     - returns: Массив с отфильтрованными объектами.
    */
    public func objectsByFilter(entityName entity: String, field: String, value: AnyObject, sort: [NSSortDescriptor]?, inContext context: NSManagedObjectContext) -> [NSManagedObject] {
        let filter = NSPredicate(format: "%K == %@", argumentArray: [field, value])
        return objectsByFilter(entityName: entity, predicate: filter, sort: sort, inContext: context)
    }
    
    /**
     Возвращает фильтрованный массив объектов.
     
     - parameter entityName: Имя сущности.
     - parameter predicate: Предикат содержащий логическое условие.
     - parameter sort: Массив объектов описывающий сортировку.
     - parameter context: Контекст в котором отрабатывает операция.
     - returns: Массив с отфильтрованными объектами.
    */
    public func objectsByFilter(entityName entity: String, predicate: NSPredicate, sort: [NSSortDescriptor]?, inContext context: NSManagedObjectContext) -> [NSManagedObject] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        request.fetchBatchSize = 50
        request.sortDescriptors = sort
        request.predicate = predicate
        return fetchRequest(request, inContext: context)
    }
    
    /**
     Возвращает найденный объект по полю и значению.
     
     - parameter entityName: Имя сущности.
     - parameter field: Поле для фильтра.
     - parameter value: Значение для фильтра.
     - returns: В случае успеха возвращает найденный объект. В другом случае nil.
    */
    public func objectByValue(entityName entity: String, field: String, value: AnyObject) -> NSManagedObject? {
        let filter = NSPredicate(format: "%K == %@", argumentArray: [field, value])
        return objectByValue(entityName: entity, predicate: filter, inContext: self.managedObjectContext)
    }
    
    /**
     Возвращает найденный объект по полю и значению.
     
     - parameter entityName: Имя сущности.
     - parameter field: Поле для фильтра.
     - parameter value: Значение для фильтра.
     - parameter context: Контекст в котором отрабатывает операция.
     - returns: В случае успеха возвращает найденный объект. В другом случае nil.
    */
    public func objectByValue(entityName entity: String, field: String, value: AnyObject, inContext context: NSManagedObjectContext) -> NSManagedObject? {
        let filter = NSPredicate(format: "%K == %@", argumentArray: [field, value])
        return objectByValue(entityName: entity, predicate: filter, inContext: context)
    }
    
    /**
     Возвращает найденный объект по полю и значению.
     
     - parameter entityName: Имя сущности.
     - parameter predicate: Предикат содержащий логическое условие.
     - returns: В случае успеха возвращает найденный объект. В другом случае nil.
    */
    public func objectByValue(entityName entity: String, predicate: NSPredicate) -> NSManagedObject? {
        return objectByValue(entityName: entity, predicate: predicate, inContext: self.managedObjectContext)
    }
    
    /**
     Возвращает найденный объект по полю и значению.
     
     - parameter entityName: Имя сущности.
     - parameter predicate: Предикат содержащий логическое условие.
     - parameter context: Контекст в котором отрабатывает операция.
     - returns: В случае успеха возвращает найденный объект. В другом случае nil.
    */
    public func objectByValue(entityName entity: String, predicate: NSPredicate, inContext context: NSManagedObjectContext) -> NSManagedObject? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        request.fetchBatchSize = 10
        request.fetchLimit = 1
        request.predicate = predicate
        return fetchRequest(request, inContext: context).first
    }
    
    fileprivate func fetchRequest(_ request: NSFetchRequest<NSFetchRequestResult>, inContext context: NSManagedObjectContext) -> [NSManagedObject] {
        var fetched: [NSManagedObject]?
        do {
            fetched = try context.fetch(request) as? [NSManagedObject]
        } catch {
            fatalError("Failed to fetch: \(error)")
        }
        
        guard let result = fetched else { return [NSManagedObject]() }
        return result
    }
    
    /**
     Сохраняет изменения.
    */
    public func saveContext() {
        do {
            try saveContext(self.managedObjectContext, completion: nil)
        } catch {
            fatalError("Failed to fetch: \(error)")
        }
    }
    
    /**
     Сохраняет изменения.
     
     - parameter context: Контекст в котором отрабатывает операция.
     - parameter completion: Блок запускается после того, как сохранение завершит свою работу.
    */
    public func saveContext(_ context: NSManagedObjectContext, completion: (() -> Void)?) throws {
        if context.hasChanges == false {
            completion?()
            return
        }
        
        try context.save()
        completion?()
    }
    
    /**
     Удаляет объект.
     
     - parameter object: объект для удаления.
    */
    public func deleteObject(_ object: NSManagedObject) {
        deleteObject(object, inContext: self.managedObjectContext)
    }
    
    /**
     Удаляет объект.
     
     - parameter object: объект для удаления.
     - parameter context: Контекст в котором отрабатывает операция.
    */
    public func deleteObject(_ object: NSManagedObject, inContext context: NSManagedObjectContext) {
        context.delete(object)
    }
    
    /**
     Очищает сущность по ее названию.
     
     - parameter entity: Имя сущности.
    */
    public func clearEntity(entityName entity: String) {
        clearEntity(entityName: entity, inContext: self.managedObjectContext)
    }
    
    /**
     Очищает сущность по ее названию.
     
     - parameter entity: Имя сущности.
     - parameter context: Контекст в котором отрабатывает операция.
    */
    public func clearEntity(entityName entity: String, inContext context: NSManagedObjectContext) {
        let results = objects(entityName: entity, sort: nil, inContext: context)
        results.forEach({ (object) -> () in
            context.delete(object)
        })
    }
    
    /**
     Очищает сущность по фильтру.
     
     - parameter entity: Имя сущности.
     - parameter field: Поле для фильтра.
     - parameter value: Значение для фильтра.
     - parameter context: Контекст в котором отрабатывает операция.
    */
    public func clearEntityWithFilter(entityName entity: String, field: String, value: AnyObject, inContext context: NSManagedObjectContext) {
        let results = objectsByFilter(entityName: entity, field: field, value: value, sort: nil, inContext: context)
        results.forEach({ (object) -> () in
            context.delete(object)
        })
    }
    
    /**
     Очищает полностью БД.
    */
    public func truncateDatabase() {
        deleteStores()
        initPersistentStore()
    }
    
    fileprivate func deleteStores() {
        let stores = self.persistentStoreCoordinator.persistentStores
        stores.forEach { (store) -> () in
            do {
                try self.persistentStoreCoordinator.remove(store)
                if let url = store.url {
                    try FileManager.default.removeItem(at: url)
                }
            } catch {
                print("Core Data Helper: ошибка при удалении БД")
            }
        }
    }
    
    
    // MARK: - Properties
    
    /**
     Контекст.
    */
    public lazy var managedObjectContext: NSManagedObjectContext = {
        [unowned self] in
        let psc = self.persistentStoreCoordinator
        let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        moc.persistentStoreCoordinator = psc
        moc.mergePolicy = NSMergePolicy(merge: .mergeByPropertyObjectTrumpMergePolicyType)
        return moc
    }()
    
    /**
     Координатор.
    */
    public lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        [unowned self] in
        let mom = self.managedObjectModel
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        return psc
    }()
    
    /**
     Модель.
    */
    public lazy var managedObjectModel: NSManagedObjectModel = {
        [unowned self] in
        let name = self.modelName
        guard let modelURL = Bundle.main.url(forResource: name, withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing manager object model from: \(modelURL)")
        }
        return mom
    }()
    
    /**
     Имя модели.
    */
    public lazy var modelName: String = {
        //TODO: Исправить, если отсутсвует запись в Info, то приложение завершиться с ошибкой
        guard let dict = Bundle.main.object(forInfoDictionaryKey: "CoreDataHelper") as? [String: String],
              let name = dict["Model"] else
        {
            fatalError("Error loading model name from plist")
        }
        
        return name
    }()
    
    /**
     Имя базы данных.
    */
    public lazy var databaseName: String = {
        //TODO: Исправить, если отсутсвует запись в Info, то приложение завершиться с ошибкой
        guard let dict = Bundle.main.object(forInfoDictionaryKey: "CoreDataHelper") as? [String: String],
              let name = dict["Database"] else
        {
            fatalError("Error loading model name from plist")
        }
        
        return name
    }()
    
    lazy var privateQueue: DispatchQueue = {
        return DispatchQueue.global(qos: .background)
    }()
    
    lazy var mainQueue: DispatchQueue = {
        return DispatchQueue.main
    }()
    
    
    // MARK: - Notification
    
    /**
     Добавляет наблюдателя для контектста.
     
     - parameter context: Контекст в котором отрабатывает операция.
    */
    public func addObserverForContext(_ context: NSManagedObjectContext) {
        NotificationCenter.default.addObserver(self, selector: #selector(CoreDataHelper.mergeContextData(_:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: context)
    }
    
    /**
     Удаляет наблюдателя для контектста.
     
     - parameter context: Контекст в котором отрабатывает операция.
    */
    public func removeObserverForContext(_ context: NSManagedObjectContext) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NSManagedObjectContextDidSave, object: context)
    }
    
    /**
     Объединяет изменения.
     
     - parameter notification: Уведомление с изменениями.
    */
    public func mergeContextData(_ notification: Notification) {
        guard let context = notification.object as? NSManagedObjectContext else { return }
        if context == self.managedObjectContext { return }

        self.mainQueue.async {
            self.managedObjectContext.mergeChanges(fromContextDidSave: notification)
            self.saveContext()
        }
    }
    
    
    // MARK: - Private
    
    fileprivate func performInitialization() {
        self.initPersistentStore()
    }
    
    fileprivate func initPersistentStore() {
        let psc = self.persistentStoreCoordinator
        let database = self.databaseName
        let storeURL = FileManager.default.applicationDocumentsDirectory()?.appendingPathComponent(database)
        
        //При необходимости этот блок можно выполнить асинхронно с privateQueue, но тогда необходимо отследить завершение загрузки
        do {
            let attributes = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: attributes)
        } catch {
            fatalError("Error migrating store: \(error)")
        }
    }
    
}
